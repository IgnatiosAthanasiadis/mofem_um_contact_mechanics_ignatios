/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifdef __cplusplus
extern "C" {
#endif
#include <cblas.h>
// #include <lapack_wrap.h>
// #include <gm_rule.h>
#include <quad.h>
#ifdef __cplusplus
}
#endif

struct ContactProblemSmallDispNoFriction : public ContactProblemKdTree {

  ContactProblemSmallDispNoFriction(
      MoFEM::Interface &m_field,
      ContactSearchKdTree::ContactCommonData_multiIndex
          &contact_commondata_multi_index,
      double &r_value_regular, double &cn_value)
      : ContactProblemKdTree(m_field, contact_commondata_multi_index),
        feActiveSetRhs(m_field, contact_commondata_multi_index),
        feActiveSetLhs(m_field, contact_commondata_multi_index),
        rValue(r_value_regular), cnValue(cn_value) {}

  ContactElement feActiveSetRhs;
  ContactElement &getLoopFeActiveSetRhs() { return feActiveSetRhs; }

  ContactElement feActiveSetLhs;
  ContactElement &getLoopFeActiveSetLhs() { return feActiveSetLhs; }

  double rValue;
  double cnValue;
  /// \biref operator to calculate and assemble Cmat for contact
  struct OpContactConstraintMatrix
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    Mat Aij;
    OpContactConstraintMatrix(const string field_name,
                              const string lagrang_field_name,
                              Mat aij = PETSC_NULL)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, field_name, UserDataOperator::OPROWCOL),
          Aij(aij) {
      sYmm = false; // This will make sure to loop over all intities (e.g. for
                    // order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN, transNN;
    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      // Both sides are needed since both sides contribute their shape function
      // to the stiffness matrix
      if (row_type == MBEDGE && row_side < 6 && row_side > 2)
        MoFEMFunctionReturnHot(0);

      const int nb_row = row_data.getIndices().size();
      if (!nb_row)
        MoFEMFunctionReturnHot(0);
      const int nb_col = col_data.getIndices().size();
      if (!nb_col)
        MoFEMFunctionReturnHot(0);
      const int nb_gauss_pts = row_data.getN().size1();

      int shift_row = 0;
      int nb_base_fun_row = row_data.getN().size2();
      if (row_type == MBVERTEX) {
        shift_row = 3;
        nb_base_fun_row = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      /*cerr << rowFieldName << endl;
      cerr << colFieldName << endl;
      cerr << row_data.getIndices() << endl;
      cerr << col_data.getIndices() << endl;*/

      int shift_col = 0;
      int nb_base_fun_col = col_data.getN().size2();
      if (col_type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      // NN is matrix first half of it belong to master/bottom tri and the
      // second half is belong to slave tri
      const double *normal_f3_ptr = &getNormalF3()[0];
      const double area_m =
          cblas_dnrm2(3, normal_f3_ptr, 1) * 0.5; // master tri area
      const double *normal_f4_ptr = &getNormalF4()[0];
      const double area_s =
          cblas_dnrm2(3, normal_f4_ptr, 1) * 0.5; // slave tri area

      // //unit normals to both f3 and f4
      VectorDouble3 n4 = getNormalF4();

      double n4_mag;
      n4_mag = sqrt(n4[0] * n4[0] + n4[1] * n4[1] + n4[2] * n4[2]);

      VectorDouble n4_unit;
      n4_unit.resize(3, false);
      for (int ii = 0; ii < 3; ii++) {
        n4_unit[ii] = n4[ii] / n4_mag;
      }

      // flag master used to identify master and slave edges and faces (used
      // to assemble edges/faces as m is -ve and s is +ve)

      // ign: no sense since these are cases that the function has already
      // returned

      bool master;
      if ((col_type == MBEDGE && col_side <= 3) ||
          (col_type == MBTRI && col_side == 3))
        master = true;
      else
        master = false;

      // this should be here outside the gauss points loops
      // we use NN[1x9 1x9] matrix for vertices as we can see all at once,
      // i.e. NN[3*nb_base_fun_row, 2*(3*nb_base_fun_col)] 1 here because only
      // one LAGMULT per node and 3 DISPLACEMENT component per node for the
      // rest we will use NN[1, 3*nb_base_fun_col]
      if (col_type == MBVERTEX) {
        // as we can see all nodes (belong to both tris) at once for MBVERTEX
        // so size of NN is (*nb_base_fun_row, 2*(3*nb_base_fun_col)
        NN.resize(nb_base_fun_row, 2 * (3 * nb_base_fun_col),
                  false); // the last false in ublas resize will destroy (not
                          // preserved) the old values
        NN.clear();
        transNN.resize(2 * (3 * nb_base_fun_col), nb_base_fun_row, false);
        transNN.clear();
      } else {
        // size of NN is different than the MBVERTEX, as we cannot see
        // entities of both(top/bottom) tris here
        NN.resize(nb_base_fun_row, 3 * nb_base_fun_col, false);
        NN.clear();
        transNN.resize(3 * nb_base_fun_col, nb_base_fun_row, false);
        transNN.clear();
      }

      // loop over half of the gauss points, as we will do all calculation
      // (belong to both bottom/top or master/slave) in this
      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        double val_s = getGaussPts()(2, gg + nb_gauss_pts / 2) * area_s;
        double val_m = getGaussPts()(2, gg) * area_m;

        // ftensor pointer pointing to the shape functions for  master and
        // slave sides (first set of gauss points belong to master and the
        // second half belong to slave)
        FTensor::Tensor0<double *> t_base_master(&col_data.getN()(gg, 0));
        FTensor::Tensor0<double *> t_base_slave(
            &col_data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        // in this if (we will calculate and assemble both m and s to the NN
        // as we can see all the nodes of the prism)
        if (col_type == MBVERTEX) {

          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            FTensor::Tensor0<double *> t_base_lambda(
                &row_data.getN()(nb_gauss_pts / 2 + gg, shift_row));

            for (int bbr = 0; bbr != nb_base_fun_row; bbr++) {
              const double m = val_m * t_base_lambda * t_base_master;
              const double s = val_s * t_base_lambda * t_base_slave;
              for (int dd = 0; dd < 3; dd++) {
                NN(bbr, 3 * bbc + dd) -= m * n4_unit[dd];
                NN(bbr, 3 * nb_base_fun_col + 3 * bbc + dd) += s * n4_unit[dd];
              }
              ++t_base_lambda; // update rows
            }
            ++t_base_master; // update cols master
            ++t_base_slave;  // update cols slave
          }
        } else { // in the else part we will either assemble m or s depending
                 // upon the entity (which can belong to either master or
                 // slave tri)
          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            FTensor::Tensor0<double *> t_base_lambda(
                &row_data.getN()(nb_gauss_pts / 2 + gg, shift_row));
            for (int bbr = 0; bbr != nb_base_fun_row; bbr++) {
              const double m = val_m * t_base_lambda * t_base_master;
              const double s = val_s * t_base_lambda * t_base_slave;

              for (int dd = 0; dd < 3; dd++) {
                if (master) {
                  NN(bbr, 3 * bbc + dd) -= m * n4_unit[dd];
                } else {
                  NN(bbr, 3 * bbc + dd) += s * n4_unit[dd];
                }
              }
              ++t_base_lambda;
            }
            ++t_base_master;
            ++t_base_slave;
          }
        }
      }

     /* for (int rr = 0; rr != nb_base_fun_row; rr++) {
        for (int cc = 0; cc != 2 * (3 * nb_base_fun_col); cc++) {
          NN(rr, cc) = 1.;
        }
      }*/

      if (Aij == PETSC_NULL) {
        Aij = getFEMethod()->snes_B;
      }

      /*printf("Displacement indices\n");
      for (int rr = 0; rr != col_data.getIndices().size(); rr++ ){
        printf("%d ", col_data.getIndices()[rr]);
}*/
//printf ("\n");
      // Calculate and assemble trans(NN) and assemble it Aij based on its
      // global indices
      noalias(transNN) = trans(NN);
      CHKERR MatSetValues(Aij, nb_col, &col_data.getIndices()[0],
                          nb_base_fun_row, &row_data.getIndices()[shift_row],
                          &*transNN.data().begin(), ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };

  struct OpGetGap
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;

    OpGetGap(const string field_name, // ign: does it matter??
             CommonDataContact &common_data_contact)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPROW),
          commonDataContact(common_data_contact) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      if (type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }

      const int nb_gauss_pts = data.getN().size1();

      commonDataContact.gAp.resize(nb_gauss_pts);
      commonDataContact.gAp.clear();

      // //unit normals to both f3 and f4
      VectorDouble3 n4 = getNormalF4();
      VectorDouble3 n4_unit(3);
      noalias(n4_unit) = n4 / norm_2(n4);

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        VectorDouble3 u_m =
            commonDataContact.dispAtGaussPts[gg]; // Displacements are stored in
                                                  // triplets at each gg
        VectorDouble3 u_s =
            commonDataContact.dispAtGaussPts[nb_gauss_pts / 2 + gg];

        // calculate gAp[at slave gauss point]+=n.(u1 - u2)
        for (int nn = 0; nn < 3; nn++) {
          commonDataContact.gAp[nb_gauss_pts / 2 + gg] -=
              n4_unit[nn] * (u_s[nn] - u_m[nn]);
        }

        commonDataContact.gAp[gg] =
            commonDataContact.gAp[nb_gauss_pts / 2 + gg];

      } // for gauss points

      MoFEMFunctionReturn(0);
    }
  };

  // Calculate displacements or spatial positions at Gauss points  (Note OPCOL
  // here, which is dips/sp-pos here)
  struct OpGetDispAtGaussPts
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    OpGetDispAtGaussPts(const string field_name,
                        CommonDataContact &common_data_contact)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPCOL),
          commonDataContact(common_data_contact) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;
      const int nb_dofs = data.getFieldData().size();

      if (type == MBEDGE && side < 6 && side > 2)
        MoFEMFunctionReturnHot(0);

      if (nb_dofs == 0)
        MoFEMFunctionReturnHot(0);

      const int nb_gauss_pts = data.getN().size1();

      // initialize, i.e. define the size of  commonDataContact.dispAtGaussPts
      if (type == MBVERTEX) {
        commonDataContact.dispAtGaussPts.resize(nb_gauss_pts);
        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          commonDataContact.dispAtGaussPts[gg].resize(
              3, false); // should be changed
          commonDataContact.dispAtGaussPts[gg].clear();
        }
      }

      // define shift_col here. as we will use it to shift the basis function
      // vector for slave side
      int shift_col = 0;
      int nb_base_fun_col = data.getN().size2();
      if (type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      // tensor index i (of size 3 due to displacement and spatial position of
      // size 3)
      FTensor::Index<'i', 3> i;

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        FTensor::Tensor0<double *> t_base_master(&data.getN()(gg, 0));
        FTensor::Tensor0<double *> t_base_slave(
            &data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        // For master gauss points [0 ~ (nb_gauss_pts/2-1)]
        FTensor::Tensor1<double *, 3> values_master(
            &commonDataContact.dispAtGaussPts[gg][0],
            &commonDataContact.dispAtGaussPts[gg][1],
            &commonDataContact.dispAtGaussPts[gg][2]);
        // For slave gauss points [nb_gauss_pts/2 ~ nb_gauss_pts]
        FTensor::Tensor1<double *, 3> values_slave(
            &commonDataContact.dispAtGaussPts[nb_gauss_pts / 2 + gg][0],
            &commonDataContact.dispAtGaussPts[nb_gauss_pts / 2 + gg][1],
            &commonDataContact.dispAtGaussPts[nb_gauss_pts / 2 + gg][2]);

        // cerr << "data.getFieldData() = "<< data.getFieldData() << endl;
        if (type == MBVERTEX) { // vertex (we see all of the 6 nodes at once,
                                // i.e. both master and slave)
          // master triangle (//the last 3 here is that we will increment this
          // tensor by 3, (which is we want))
          FTensor::Tensor1<double *, 3> t_field_data_master(
              &data.getFieldData()[0], &data.getFieldData()[1],
              &data.getFieldData()[2], 3);
          // slave triangle
          FTensor::Tensor1<double *, 3> t_field_data_slave(
              &data.getFieldData()[9], &data.getFieldData()[10],
              &data.getFieldData()[11], 3); // in-between
          // 3 lower and 3 upper nodes (both in one loop)
          for (int bb = 0; bb != nb_base_fun_col; bb++) {
            values_master(i) += t_base_master * t_field_data_master(i);
            values_slave(i) += t_base_slave * t_field_data_slave(i);
            ++t_base_master;
            ++t_field_data_master;
            ++t_base_slave;
            ++t_field_data_slave;
          }

        } else if ((type == MBEDGE && side <= 3) ||
                   (type == MBTRI &&
                    side == 3)) { // master tri (here we see only master sides)
          // update only master
          FTensor::Tensor1<double *, 3> t_field_data(
              &data.getFieldData()[0], &data.getFieldData()[1],
              &data.getFieldData()[2], 3);
          for (int bb = 0; bb != nb_base_fun_col; bb++) {
            values_master(i) += t_base_master * t_field_data(i);
            ++t_base_master;
            ++t_field_data;
          }
        } else if ((type == MBEDGE && side >= 6) ||
                   (type == MBTRI &&
                    side == 4)) { // slave tri (here we see only slave side)
          FTensor::Tensor1<double *, 3> t_field_data(
              &data.getFieldData()[0], &data.getFieldData()[1],
              &data.getFieldData()[2], 3);
          // update only slave side
          for (int bb = 0; bb != nb_base_fun_col; bb++) {
            values_slave(i) += t_base_slave * t_field_data(i);
            ++t_base_slave;
            ++t_field_data;
          }
        }
      }
      MoFEMFunctionReturn(0);
    }
  };

  /**
   * Operator to calculate vector Fg, which will tells us that the dof is either
   * active (+ve) or inactive (-ve)
   */
  struct OpCalTildeCFun
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    double r;  //@todo: ign: to become input parameter
    double cN; //@todo: ign: to become input parameter

    OpCalTildeCFun(const string lagrang_field_name, // ign: does it matter?
                   CommonDataContact &common_data_contact, double &r_value,
                   double &cn_value)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, UserDataOperator::OPROW),
          commonDataContact(common_data_contact), r(r_value), cN(cn_value) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      if (type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }

      const int nb_gauss_pts = data.getN().size1();

      commonDataContact.tildeCFun.resize(nb_gauss_pts);
      commonDataContact.tildeCFun.clear();

      commonDataContact.lambdaGapDiffProduct.resize(nb_gauss_pts);
      commonDataContact.lambdaGapDiffProduct.clear();

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        const double cg = cN * commonDataContact.gAp[gg + nb_gauss_pts / 2];
        const double lambda =
            commonDataContact.lagMultAtGaussPts[gg + nb_gauss_pts / 2][0];
        const double regular_abs = abs(lambda - cg);

        commonDataContact.tildeCFun[gg + nb_gauss_pts / 2] =
            0.5 *
            (lambda + cg - pow(regular_abs, r) / r); // is lagMult Correct?

        commonDataContact.tildeCFun[gg] =
            commonDataContact.tildeCFun[gg + nb_gauss_pts / 2];

        const double lambda_gap_diff = lambda - cg;
        const double exponent = r - 1.;

        double &res =
            commonDataContact.lambdaGapDiffProduct[gg + nb_gauss_pts / 2];

        double sign = 0.;
        sign = (lambda_gap_diff == 0) ? 0 : (lambda_gap_diff < 0) ? -1 : 1;
        res = sign * pow(abs(lambda_gap_diff), exponent);
        commonDataContact.lambdaGapDiffProduct[gg] = res;
      }
      MoFEMFunctionReturn(0);
    }
  };

  struct OpCalFReCon
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;

    OpCalFReCon(const string field_name, CommonDataContact &common_data_contact)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPCOL),
          commonDataContact(common_data_contact) {}

    VectorDouble vec_f;
    PetscErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      PetscFunctionBegin;

      // cout<<" OpCal_fcon "<<endl;

      if (type == MBEDGE && side < 6 && side > 2)
        MoFEMFunctionReturnHot(0);

      if (data.getIndices().size() == 0)
        PetscFunctionReturn(0);

      const int nb_gauss_pts = data.getN().size1();
      int shift_col = 0;
      int nb_base_fun_col = data.getN().size2();
      if (type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      // flag master used to identify master and slave edges and faces (used to
      // assemble edges/faces as m is -ve and s is +ve)
      bool master;
      if ((type == MBEDGE && side <= 3) || (type == MBTRI && side == 3))
        master = true;
      else
        master = false;

      if (type == MBVERTEX) {
        vec_f.resize(2 * 3 * nb_base_fun_col,
                     false); // the last false in ublas
                             // resize will destroy (not
                             // preserved) the old
                             // values
        vec_f.clear();
      } else {
        vec_f.resize(3 * nb_base_fun_col, false);
        vec_f.clear();
      }

      // VectorDouble3 normal_f4 = getNormalF4();
      VectorDouble3 n4 = getNormalF4();

      double n4_mag;
      n4_mag = sqrt(n4[0] * n4[0] + n4[1] * n4[1] + n4[2] * n4[2]);

      VectorDouble n4_unit;
      n4_unit.resize(3, false);
      for (int ii = 0; ii < 3; ii++) {
        n4_unit[ii] = n4[ii] / n4_mag;
      }

      const double *normal_f3_ptr = &getNormalF3()[0];
      const double area_m =
          cblas_dnrm2(3, normal_f3_ptr, 1) * 0.5; // master tri area
      const double *normal_f4_ptr = &getNormalF4()[0];
      const double area_s =
          cblas_dnrm2(3, normal_f4_ptr, 1) * 0.5; // slave tri area

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {

        // due to integration over the parent element, do not need to multiply
        // with jacobian (area)

        // multiply weight with 0.5 due to 1/2 in the original equations
        double val_s = getGaussPts()(2, gg + nb_gauss_pts / 2) * area_s;
        double val_m = getGaussPts()(2, gg) * area_m;

        FTensor::Tensor0<double *> t_base_master(&data.getN()(gg, 0));
        FTensor::Tensor0<double *> t_base_slave(
            &data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        const double lambda =
            commonDataContact.lagMultAtGaussPts[nb_gauss_pts / 2 + gg][0];

        if (type == MBVERTEX) {
          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            for (int nn = 0; nn != 3; nn++) {
              const double m = val_m * t_base_master * n4_unit[nn] * lambda;
              const double s = val_s * t_base_slave * n4_unit[nn] * lambda;
              vec_f[3 * bbc + nn] -= m;
              vec_f[3 * nb_base_fun_col + 3 * bbc + nn] += s;
            }
            ++t_base_master;
            ++t_base_slave;
          }
        } else {
          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            for (int nn = 0; nn != 3; nn++) {
              if (master) {
                const double m = val_m * t_base_master * n4_unit[nn] * lambda;
                vec_f(3 * bbc + nn) -= m;
              } else {
                const double s = val_s * t_base_slave * n4_unit[nn] * lambda;
                vec_f(3 * bbc + nn) += s;
              }
            }
            ++t_base_master;
            ++t_base_slave;
          }
        }

      } // for gauss points

      const int nb_col = data.getIndices().size();
      CHKERR VecSetValues(getFEMethod()->snes_f, nb_col, &data.getIndices()[0],
                          &vec_f[0], ADD_VALUES);
      PetscFunctionReturn(0);
    }
  };

  /**
   * Operator to calculate vector Fg, which will tells us that the dof is either
   * active (+ve) or inactive (-ve)
   */
  struct OpCalIntTildeCFun
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    // Vec residual;
    OpCalIntTildeCFun(const string lagrang_field_name,
                      CommonDataContact &common_data_contact)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, UserDataOperator::OPCOL),
          commonDataContact(common_data_contact) {}

    VectorDouble vecR;
    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      if (data.getIndices().size() == 0)
        MoFEMFunctionReturnHot(0);
      // ignore first triangle (LagMul only exists on slave (triangle 4))
      if (type == MBEDGE && side < 6)
        MoFEMFunctionReturnHot(0);
      if (type == MBTRI && side == 3)
        MoFEMFunctionReturnHot(0);

      const int nb_gauss_pts = data.getN().size1();

      int shift_col = 0;
      int nb_base_fun_col = data.getN().size2();
      if (type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }
      // NN is matrix first half of it belong to master/bottom tri and the
      // second half is belong to slave tri
      const double *normal_f3_ptr = &getNormalF3()[0];

      const double *normal_f4_ptr = &getNormalF4()[0];
      const double area_s =
          cblas_dnrm2(3, normal_f4_ptr, 1) * 0.5; // slave tri area

      // as we can see all nodes (belong to both tris) but LagMul only exists on
      // the slave sides
      vecR.resize(nb_base_fun_col, false); // the last false in ublas resize
                                           // will destroy (not preserved) the
                                           // old values
      vecR.clear();

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        double val_s = getGaussPts()(2, gg + nb_gauss_pts / 2) * area_s;
        // in this if (we will calculate and assemble both m and s to the NN
        // as we can see all the nodes of the prism)

        FTensor::Tensor0<double *> t_base_lambda(
            &data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        VectorDouble3 n4 = getNormalF4();
        VectorDouble3 n4_unit(3);
        noalias(n4_unit) = n4 / norm_2(n4);

        for (int bbr = 0; bbr != nb_base_fun_col; bbr++) {
          const double s = val_s * t_base_lambda *
                           commonDataContact.tildeCFun[gg + nb_gauss_pts / 2];
          vecR[bbr] += s;  // it is a plus since F always scales with -1
          ++t_base_lambda; // update rows
        }

      } // for gauss points
      CHKERR VecSetValues(getFEMethod()->snes_f, nb_base_fun_col,
                          &data.getIndices()[shift_col], &vecR[0], ADD_VALUES);
      MoFEMFunctionReturn(0);
    }
  };

  // setup operators for calculation of active set
  MoFEMErrorCode setContactOperatorsActiveSet(string field_name,
                                              string lagrang_field_name) {
    MoFEMFunctionBegin;
    cout << "Hi 1 from setContactOperatorsActiveSet " << endl;

    map<int, ContactPrismsData>::iterator sit = setOfContactPrism.begin();
    for (; sit != setOfContactPrism.end(); sit++) {

      // RHS
      // calculate displacement/spatial position at Gauss point
      feActiveSetRhs.getOpPtrVector().push_back(
          new OpGetDispAtGaussPts(field_name, commonDataContact));

      // ign:evaluate
      feActiveSetRhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpGetGap(field_name,
                                                          commonDataContact));

      // //calculte LagMul at Gauss point
      feActiveSetRhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::
              OpGetLagMulAtGaussPts( // Not
                                     // tested
                  lagrang_field_name, commonDataContact));

      // ign:evaluate
      feActiveSetRhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpCalTildeCFun( // Not tested
              field_name, commonDataContact, rValue, cnValue));

      feActiveSetRhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpCalFReCon(
              field_name, commonDataContact));

      // integrate and assemble tildeCFun function intTildeCFun = integral( Nlam
      // * TildeCFun)
      feActiveSetRhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpCalIntTildeCFun(
              lagrang_field_name, commonDataContact));
    }
    MoFEMFunctionReturn(0);
  }

  struct OpGetLagMulAtGaussPts
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    CommonDataContact &commonDataContact;
    OpGetLagMulAtGaussPts(const string lagrang_field_name,
                          CommonDataContact &common_data_contact)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, UserDataOperator::OPROW),
          commonDataContact(common_data_contact) {}

    int rank;
    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      // As lagrange multipliers are only sitting on the slave/top side
      if (type == MBEDGE && side < 6)
        MoFEMFunctionReturnHot(0);
      if (type == MBTRI && side == 3)
        MoFEMFunctionReturnHot(0);

      const int nb_gauss_pts = data.getN().size1();

      // initialize, i.e. define the size of  commonDataContact.dispAtGaussPts
      rank = data.getFieldData().size() / 6;

      if (type == MBVERTEX) {
        commonDataContact.lagMultAtGaussPts.resize(nb_gauss_pts);
        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          commonDataContact.lagMultAtGaussPts[gg].resize(rank, false);
          commonDataContact.lagMultAtGaussPts[gg].clear();
        }
      }

      int shift_row = 0;
      int nb_base_fun_row = data.getN().size2();
      if (type == MBVERTEX) {
        shift_row = 3;
        nb_base_fun_row = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        FTensor::Tensor0<double *> t_base_lambda(
            &data.getN()(nb_gauss_pts / 2 + gg, shift_row));
        // For slave gauss points [nb_gauss_pts/2 ~ nb_gauss_pts]
        // same lagrange multipliers on both sides
        FTensor::Tensor0<double *> values_slave(
            &commonDataContact.lagMultAtGaussPts[nb_gauss_pts / 2 + gg][0]);
        if (type == MBVERTEX) {
          FTensor::Tensor0<double *> t_field_data_slave(
              &data.getFieldData()[3]);
          // 3 lower and 3 upper nodes (both in one loop)
          for (int bb = 0; bb != nb_base_fun_row; bb++) {
            values_slave += t_base_lambda * t_field_data_slave;
            ++t_base_lambda;
            ++t_field_data_slave;
          }
        } else {
          FTensor::Tensor0<double *> t_field_data_slave(
              &data.getFieldData()[0]);
          // 3 lower and 3 upper nodes (both in one loop)
          for (int bb = 0; bb != nb_base_fun_row; bb++) {
            values_slave += t_base_lambda * t_field_data_slave;
            ++t_base_lambda;
            ++t_field_data_slave;
          }
        }

        commonDataContact.lagMultAtGaussPts[gg] =
            commonDataContact.lagMultAtGaussPts[nb_gauss_pts / 2 + gg];
      }

      MoFEMFunctionReturn(0);
    }
  };

  /// \biref operator to calculate and assemble Cmat for contact
  struct OpDerivativeBarTildeCFunOLambda
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    Mat Aij;
    CommonDataContact &commonDataContact;
    OpDerivativeBarTildeCFunOLambda(const string lagrang_field_name,
                                    CommonDataContact &common_data_contact,
                                    Mat aij = PETSC_NULL)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, UserDataOperator::OPROWCOL),
          Aij(aij), commonDataContact(common_data_contact) {
      sYmm = false; // This will make sure to loop over all entities (e.g. for
                    // order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN;
    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      // As lagrange multipliers are only sitting on the slave/top side
      if (row_type == MBEDGE && row_side < 6)
        MoFEMFunctionReturnHot(0);
      if (row_type == MBTRI && row_side == 3)
        MoFEMFunctionReturnHot(0);

      const int nb_row = row_data.getIndices().size();
      if (!nb_row)
        MoFEMFunctionReturnHot(0);
      const int nb_col = col_data.getIndices().size();
      if (!nb_col)
        MoFEMFunctionReturnHot(0);
      const int nb_gauss_pts = row_data.getN().size1();

      int shift_row = 0;
      int nb_base_fun_row = row_data.getN().size2();
      if (row_type == MBVERTEX) {
        shift_row = 3;
        nb_base_fun_row = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      int shift_col = 0;
      int nb_base_fun_col = col_data.getN().size2();
      if (col_type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      // NN is matrix first half of it belong to master/bottom tri and the
      // second half is belong to slave tri
      const double *normal_f4_ptr = &getNormalF4()[0];
      const double area_s =
          cblas_dnrm2(3, normal_f4_ptr, 1) * 0.5; // slave tri area

      // flag master used to identify master and slave edges and faces (used
      // to assemble edges/faces as m is -ve and s is +ve)

      // only lagrange multipliers are taken into account
      // there is only one lagrange multiplier per physical dof

      NN.resize(nb_base_fun_row, nb_base_fun_col,
                false); // the last false in ublas resize will destroy (not
                        // preserved) the old values
      NN.clear();
     
      // loop over half of the gauss points, as we will do all calculation
      // (belong to both bottom/top or master/slave) in this
      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        const double val_s = getGaussPts()(2, gg + nb_gauss_pts / 2) * area_s;

        const double lambda_gap_diff_prod =
            commonDataContact.lambdaGapDiffProduct[gg + nb_gauss_pts / 2];

        // outer lambda
        FTensor::Tensor0<double *> t_base_lambda_col(
            &col_data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        // operating only on slave side so half the base functions are taken
        // into account

        for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
          FTensor::Tensor0<double *> t_base_lambda_row(
              &row_data.getN()(nb_gauss_pts / 2 + gg, shift_row));
          for (int bbr = 0; bbr != nb_base_fun_row; bbr++) {
            NN(bbr, bbc) += 0.5 * (1 - lambda_gap_diff_prod) * val_s *
                            t_base_lambda_row * t_base_lambda_col;
            ++t_base_lambda_row;
          }
          ++t_base_lambda_col; // update rows
        }
        
      }

      if (Aij == PETSC_NULL) {
        Aij = getFEMethod()->snes_B;
      }

      // Assemble NN to final Aij vector based on its global indices
      CHKERR MatSetValues(
          Aij, nb_base_fun_row, &row_data.getIndices()[shift_row],
          nb_base_fun_col, // ign: is shift row right here?
          &col_data.getIndices()[shift_col], &*NN.data().begin(), ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };

  /// \biref operator to calculate and assemble Cmat for contact
  struct OpDerivativeBarTildeCFunODisplacements
      : public FlatPrismElementForcesAndSourcesCore::UserDataOperator {

    double cN; //@todo: ign: to become input parameter
    Mat Aij;
    CommonDataContact &commonDataContact;
    OpDerivativeBarTildeCFunODisplacements(
        const string field_name, const string lagrang_field_name,
        double &cn_value, CommonDataContact &common_data_contact,
        Mat aij = PETSC_NULL)
        : FlatPrismElementForcesAndSourcesCore::UserDataOperator(
              lagrang_field_name, field_name, UserDataOperator::OPROWCOL),
          cN(cn_value), Aij(aij), commonDataContact(common_data_contact) {
      sYmm = false; // This will make sure to loop over all entities (e.g. for
                    // order=2 it will make doWork to loop 16 time)
    }
    MatrixDouble NN;
    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      if (row_type == MBEDGE && row_side < 6 && row_side > 2)
        MoFEMFunctionReturnHot(0);

      const int nb_row = row_data.getIndices().size();
      if (!nb_row)
        MoFEMFunctionReturnHot(0);
      const int nb_col = col_data.getIndices().size();
      if (!nb_col)
        MoFEMFunctionReturnHot(0);
      const int nb_gauss_pts = row_data.getN().size1();

      int shift_row = 0;
      int nb_base_fun_row = row_data.getN().size2();
      if (row_type == MBVERTEX) {
        shift_row = 3;
        nb_base_fun_row = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      int shift_col = 0;
      int nb_base_fun_col = col_data.getN().size2();
      if (col_type == MBVERTEX) {
        shift_col = 3;
        nb_base_fun_col = 3; // total are 6 for MBVERTEX (top and bottom tris)
      }

      // NN is matrix first half of it belong to master/bottom tri and the
      // second half is belong to slave tri
      const double *normal_f3_ptr = &getNormalF3()[0];
      const double area_m =
          cblas_dnrm2(3, normal_f3_ptr, 1) * 0.5; // master tri area
      const double *normal_f4_ptr = &getNormalF4()[0];
      const double area_s =
          cblas_dnrm2(3, normal_f4_ptr, 1) * 0.5; // slave tri area

      // //unit normals to both f3 and f4
      VectorDouble n4 = getNormalF4();

      double n4_mag;
      n4_mag = sqrt(n4[0] * n4[0] + n4[1] * n4[1] + n4[2] * n4[2]);

      VectorDouble n4_unit;
      n4_unit.resize(3, false);
      for (int ii = 0; ii < 3; ii++) {
        n4_unit[ii] = n4[ii] / n4_mag;
      }

      // flag master used to identify master and slave edges and faces (used
      // to assemble edges/faces as m is -ve and s is +ve)

      // ign: no sense since these are cases that the function has already
      // returned

      bool master;
      if ((col_type == MBEDGE && col_side <= 3) ||
          (col_type == MBTRI && col_side == 3))
        master = true;
      else
        master = false;

      // this should be here outside the gauss points loops
      // we use NN[1x9 1x9] matrix for vertices as we can see all at once,
      // i.e. NN[3*nb_base_fun_row, 2*(3*nb_base_fun_col)] 1 here because only
      // one LAGMULT per node and 3 DISPLACEMENT component per node for the
      // rest we will use NN[1, 3*nb_base_fun_col]
      if (col_type == MBVERTEX) {
        // as we can see all nodes (belong to both tris) at once for MBVERTEX
        // so size of NN is (*nb_base_fun_row, 2*(3*nb_base_fun_col)
        NN.resize(nb_base_fun_row, 2 * (3 * nb_base_fun_col),
                  false); // the last false in ublas resize will destroy (not
                          // preserved) the old values
        NN.clear();
      } else {
        // size of NN is different than the MBVERTEX, as we cannot see
        // entities of both(top/bottom) tris here
        NN.resize(nb_base_fun_row, 3 * nb_base_fun_col, false);
        NN.clear();
      }

      // loop over half of the gauss points, as we will do all calculation
      // (belong to both bottom/top or master/slave) in this
      for (int gg = 0; gg != nb_gauss_pts / 2; gg++) {
        double val_s = getGaussPts()(2, gg + nb_gauss_pts / 2) * area_s;
        double val_m = getGaussPts()(2, gg) * area_m;

        // ftensor pointer pointing to the shape functions for  master and
        // slave sides (first set of gauss points belong to master and the
        // second half belong to slave)
        FTensor::Tensor0<double *> t_base_master(&col_data.getN()(gg, 0));
        FTensor::Tensor0<double *> t_base_slave(
            &col_data.getN()(nb_gauss_pts / 2 + gg, shift_col));

        const double prod =
            commonDataContact.lambdaGapDiffProduct[gg + nb_gauss_pts / 2];

        // in this if (we will calculate and assemble both m and s to the NN
        // as we can see all the nodes of the prism)
        if (col_type == MBVERTEX) {

          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            FTensor::Tensor0<double *> t_base_lambda(
                &row_data.getN()(nb_gauss_pts / 2 + gg, shift_row));

            for (int bbr = 0; bbr != nb_base_fun_row; bbr++) {
              const double m = val_m * t_base_lambda * t_base_master;
              const double s = val_s * t_base_lambda * t_base_slave;

              for (int dd = 0; dd < 3; dd++) {

                NN(bbr, 3 * bbc + dd) +=
                    0.5 * cN * (1 + prod) * m * n4_unit[dd];
                NN(bbr, 3 * nb_base_fun_col + 3 * bbc + dd) -=
                    0.5 * cN * (1 + prod) * s * n4_unit[dd];
              }
              ++t_base_lambda; // update rows
            }
            ++t_base_master; // update cols master
            ++t_base_slave;  // update cols slave
          }
        } else { // in the else part we will either assemble m or s depending
                 // upon the entity (which can belong to either master or
                 // slave tri)
          for (int bbc = 0; bbc != nb_base_fun_col; bbc++) {
            FTensor::Tensor0<double *> t_base_lambda(
                &row_data.getN()(nb_gauss_pts / 2 + gg, shift_row));
            for (int bbr = 0; bbr != nb_base_fun_row; bbr++) {
              const double m = val_m * t_base_lambda * t_base_master;
              const double s = val_s * t_base_lambda * t_base_slave;

              for (int dd = 0; dd < 3; dd++) {
                if (master) {
                  NN(bbr, 3 * bbc + dd) +=
                      0.5 * cN * (1 + prod) * m * n4_unit[dd];
                } else {
                  NN(bbr, 3 * bbc + dd) -=
                      0.5 * cN * (1 + prod) * s * n4_unit[dd];
                }
              }
              ++t_base_lambda;
            }
            ++t_base_master;
            ++t_base_slave;
          }
        }
      }
      

      if (Aij == PETSC_NULL) {
        Aij = getFEMethod()->snes_B;
      }

      // Assemble NN to final Aij vector based on its global indices
      CHKERR MatSetValues(
          Aij, nb_base_fun_row, &row_data.getIndices()[shift_row], nb_col,
          &col_data.getIndices()[0], &*NN.data().begin(), ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };

  MoFEMErrorCode
  setContactOperators(string field_name, string lagrang_field_name, Mat aij,
                      ContactSearchKdTree::ContactCommonData_multiIndex
                          &contact_commondata_multi_index,
                      double &area_master, double &area_slave) {
    MoFEMFunctionBegin;
    cout << "Hi 1 from setRVEBCsOperators " << endl;
    cout << "setOfContactPrism[1].pRisms " << setOfContactPrism[1].pRisms
         << endl;

    map<int, ContactPrismsData>::iterator sit = setOfContactPrism.begin();
    for (; sit != setOfContactPrism.end(); sit++) {
      feActiveSetLhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpContactConstraintMatrix(
              field_name, lagrang_field_name, aij));

      // calculate displacement/spatial position at Gauss point
      feActiveSetLhs.getOpPtrVector().push_back(
          new OpGetDispAtGaussPts(field_name, commonDataContact));

      // evaluate gap
      feActiveSetLhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::OpGetGap(field_name,
                                                          commonDataContact));

      // //calculate LagMul at Gauss point
      feActiveSetLhs.getOpPtrVector().push_back(
          new OpGetLagMulAtGaussPts(lagrang_field_name, commonDataContact));

      //evaluate complementary function
      feActiveSetLhs.getOpPtrVector().push_back(
          new OpCalTildeCFun(field_name, commonDataContact, rValue, cnValue));

      //evaluate remaining blocks of tangent matrix
      feActiveSetLhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::
              OpDerivativeBarTildeCFunOLambda(lagrang_field_name,
                                              commonDataContact, aij));

      feActiveSetLhs.getOpPtrVector().push_back(
          new ContactProblemSmallDispNoFriction::
              OpDerivativeBarTildeCFunODisplacements(
                  field_name, lagrang_field_name, cnValue, commonDataContact,
                  aij));
      
    }
    MoFEMFunctionReturn(0);
  }
};