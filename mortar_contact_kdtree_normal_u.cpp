/* This file is part of MoFEM.
 * \ingroup nonlinear_elastic_elem

 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
using namespace MoFEM;

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;
#include <Hooke.hpp>
using namespace boost::numeric;

#include "clipper.hpp"
using namespace ClipperLib;

#include "ContactSearchKdTree.hpp"
#include "moab/AdaptiveKDTree.hpp"
#include <ContactProblemKdTree.hpp>
#include <ContactProblemKdTreeNormalU.hpp>
#include <ContactProblemSmallDispNoFriction.hpp>

static char help[] = "-my_block_config set block data\n"
                     "\n";

struct BlockOptionData {
  int oRder;
  double yOung;
  double pOisson;
  double initTemp;
  BlockOptionData() : oRder(-1), yOung(-1), pOisson(-2), initTemp(0) {}
};

int main(int argc, char *argv[]) {
  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);
  moab::Core mb_instance;
  moab::Interface &moab = mb_instance;

  PetscBool flg_block_config, flg_file;
  char mesh_file_name[255];
  char block_config_file[255];
  PetscInt order = 2;
  PetscBool is_partitioned = PETSC_FALSE;
  PetscReal r_value = 1.01;
  PetscReal cn_value = 0.;
  PetscInt nb_sub_steps = 10;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "Elastic Config", "none");
  CHKERR PetscOptionsString("-my_file", "mesh file name", "", "mesh.h5m",
                            mesh_file_name, 255, &flg_file);

  CHKERR PetscOptionsInt("-my_order", "default approximation order", "", 1,
                         &order, PETSC_NULL);

  CHKERR PetscOptionsReal("-my_r_value", "default regularisation r value", "",
                          1.01, &r_value, PETSC_NULL);

  CHKERR PetscOptionsReal("-my_cn_value", "default regularisation cn value", "",
                          1., &cn_value, PETSC_NULL);

  CHKERR PetscOptionsBool("-my_is_partitioned",
                          "set if mesh is partitioned (this result that each "
                          "process keeps only part of the mes",
                          "", PETSC_FALSE, &is_partitioned, PETSC_NULL);
  CHKERR PetscOptionsString("-my_block_config", "elastic configure file name",
                            "", "block_conf.in", block_config_file, 255,
                            &flg_block_config);
  CHKERR PetscOptionsInt("-my_nb_sub_steps", "number of substeps", "", 10,
                         &nb_sub_steps, PETSC_NULL);

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  // Read parameters from line command
  if (flg_file != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
  }

  ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
  if (pcomm == NULL)
    pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

  if (is_partitioned == PETSC_TRUE) {
    // Read mesh to MOAB
    const char *option;
    option = "PARALLEL=BCAST_DELETE;"
             "PARALLEL_RESOLVE_SHARED_ENTS;"
             "PARTITION=PARALLEL_PARTITION;";
    CHKERR moab.load_file(mesh_file_name, 0, option);
  } else {
    const char *option;
    option = "";
    CHKERR moab.load_file(mesh_file_name, 0, option);
  }

  // Create MoFEM (Joseph) database
  MoFEM::Core core(moab);
  MoFEM::Interface &m_field = core;

  // print bcs
  MeshsetsManager *mmanager_ptr;
  CHKERR m_field.getInterface(mmanager_ptr);
  CHKERR mmanager_ptr->printDisplacementSet();
  CHKERR mmanager_ptr->printForceSet();
  // print block sets with materials
  CHKERR mmanager_ptr->printMaterialsSet();

  // stl::bitset see for more details
  BitRefLevel bit_level0;
  bit_level0.set(0);
  CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(0, 3,
                                                                    bit_level0);

  Range meshset_level0;
  CHKERR m_field.getInterface<BitRefManager>()->getEntitiesByRefLevel(
      bit_level0, BitRefLevel().set(), meshset_level0);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD, "meshset_level0 %d\n",
                          meshset_level0.size());
  PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);

  Range range_surf_master, range_surf_slave;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, SIDESET, it)) {
    if (it->getName().compare(0, 6, "Master") == 0) {
      CHKERR m_field.get_moab().get_entities_by_type(it->meshset, MBTRI,
                                                     range_surf_master, true);
    }
  }
  cout << "range_surf_master = " << range_surf_master.size() << endl;

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, SIDESET, it)) {
    if (it->getName().compare(0, 5, "Slave") == 0) {
      CHKERR m_field.get_moab().get_entities_by_type(it->meshset, MBTRI,
                                                     range_surf_slave, true);
    }
  }
  cout << "range_surf_slave = " << range_surf_slave.size() << endl;

  EntityHandle meshset_surf_slave, meshset_surf_master;
  CHKERR moab.create_meshset(MESHSET_SET, meshset_surf_slave);

  CHKERR moab.create_meshset(MESHSET_SET, meshset_surf_master);

  CHKERR moab.add_entities(meshset_surf_slave, range_surf_slave);

  CHKERR moab.add_entities(meshset_surf_master, range_surf_master);

  CHKERR moab.write_mesh("surf_slave.vtk", &meshset_surf_slave, 1);
  CHKERR moab.write_mesh("surf_master.vtk", &meshset_surf_master, 1);

  EntityHandle meshset_tri_slave, out_put_set, meshset_polygons;
  CHKERR moab.create_meshset(MESHSET_SET, meshset_tri_slave);

  CHKERR moab.create_meshset(MESHSET_SET, out_put_set);

  CHKERR moab.create_meshset(MESHSET_SET, meshset_polygons);

  // Define problem
  // Fields
  CHKERR m_field.add_field("DISPLACEMENT", H1, AINSWORTH_LEGENDRE_BASE, 3,
                           MB_TAG_SPARSE, MF_ZERO);

  //   for normal displacement (rank 1)
  CHKERR m_field.add_field("LAGMULT", H1, AINSWORTH_LEGENDRE_BASE, 1,
                           MB_TAG_SPARSE, MF_ZERO);

  // Declare problem
  // add entities (by tets) to the field
  CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "DISPLACEMENT");
  CHKERR m_field.add_ents_to_field_by_type(range_surf_slave, MBTRI, "LAGMULT");

  // set app. order
  // see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes
  // (Mark Ainsworth & Joe Coyle)
  CHKERR m_field.set_field_order(0, MBTET, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBTRI, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBEDGE, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBVERTEX, "DISPLACEMENT", 1);

  CHKERR m_field.set_field_order(0, MBTRI, "LAGMULT", order);
  CHKERR m_field.set_field_order(0, MBEDGE, "LAGMULT", order);
  CHKERR m_field.set_field_order(0, MBVERTEX, "LAGMULT", 1);

  // configure blocks by parsing config file
  // it allow to set approximation order for each block independently
  std::map<int, BlockOptionData> block_data;
  if (flg_block_config) {
    try {
      ifstream ini_file(block_config_file);

      po::variables_map vm;
      po::options_description config_file_options;
      for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
        std::ostringstream str_order;
        str_order << "block_" << it->getMeshsetId() << ".displacement_order";
        config_file_options.add_options()(
            str_order.str().c_str(),
            po::value<int>(&block_data[it->getMeshsetId()].oRder)
                ->default_value(order));
        std::ostringstream str_cond;
        str_cond << "block_" << it->getMeshsetId() << ".young_modulus";
        config_file_options.add_options()(
            str_cond.str().c_str(),
            po::value<double>(&block_data[it->getMeshsetId()].yOung)
                ->default_value(-1));
        std::ostringstream str_capa;
        str_capa << "block_" << it->getMeshsetId() << ".poisson_ratio";
        config_file_options.add_options()(
            str_capa.str().c_str(),
            po::value<double>(&block_data[it->getMeshsetId()].pOisson)
                ->default_value(-2));
        std::ostringstream str_init_temp;
        str_init_temp << "block_" << it->getMeshsetId()
                      << ".initial_temperature";
        config_file_options.add_options()(
            str_init_temp.str().c_str(),
            po::value<double>(&block_data[it->getMeshsetId()].initTemp)
                ->default_value(0));
      }
      po::parsed_options parsed =
          parse_config_file(ini_file, config_file_options, true);
      store(parsed, vm);
      po::notify(vm);
      for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
        if (block_data[it->getMeshsetId()].oRder == -1)
          continue;
        if (block_data[it->getMeshsetId()].oRder == order)
          continue;
        PetscPrintf(PETSC_COMM_WORLD, "Set block %d order to %d\n",
                    it->getMeshsetId(), block_data[it->getMeshsetId()].oRder);
        Range block_ents;
        CHKERR moab.get_entities_by_handle(it->getMeshset(), block_ents, true);

        Range ents_to_set_order;
        CHKERR moab.get_adjacencies(block_ents, 3, false, ents_to_set_order,
                                    moab::Interface::UNION);

        ents_to_set_order = ents_to_set_order.subset_by_type(MBTET);
        CHKERR moab.get_adjacencies(block_ents, 2, false, ents_to_set_order,
                                    moab::Interface::UNION);

        CHKERR moab.get_adjacencies(block_ents, 1, false, ents_to_set_order,
                                    moab::Interface::UNION);

        CHKERR m_field.synchronise_entities(ents_to_set_order);
        CHKERR m_field.set_field_order(ents_to_set_order, "DISPLACEMENT",
                                       block_data[it->getMeshsetId()].oRder);
      }
      std::vector<std::string> additional_parameters;
      additional_parameters =
          collect_unrecognized(parsed.options, po::include_positional);
      for (std::vector<std::string>::iterator vit =
               additional_parameters.begin();
           vit != additional_parameters.end(); ++vit) {
        CHKERR PetscPrintf(PETSC_COMM_WORLD,
                           "** WARNING Unrecognized option %s\n", vit->c_str());
      }
    } catch (const std::exception &ex) {
      std::ostringstream ss;
      ss << ex.what() << std::endl;
      SETERRQ(PETSC_COMM_SELF, MOFEM_STD_EXCEPTION_THROW, ss.str().c_str());
    }
  }

  // Add elastic element
  boost::shared_ptr<Hooke<adouble>> hooke_adouble_ptr(new Hooke<adouble>());
  boost::shared_ptr<Hooke<double>> hooke_double_ptr(new Hooke<double>());
  NonlinearElasticElement elastic(m_field, 2);
  CHKERR elastic.setBlocks(hooke_double_ptr, hooke_adouble_ptr);
  CHKERR elastic.addElement("ELASTIC", "DISPLACEMENT");
  
  CHKERR elastic.setOperators("DISPLACEMENT", "MESH_NODE_POSITIONS", false,
                              true);

  // Update material parameters
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
    int id = it->getMeshsetId();
    if (cn_value == 0)
       cn_value = elastic.setOfBlocks[id].E;
      if (block_data[id].yOung > 0) {
        elastic.setOfBlocks[id].E = block_data[id].yOung;
        CHKERR PetscPrintf(PETSC_COMM_WORLD,
                           "Block %d set Young modulus %3.4g\n", id,
                           elastic.setOfBlocks[id].E);
      }
    if (block_data[id].pOisson >= -1) {
      elastic.setOfBlocks[id].PoissonRatio = block_data[id].pOisson;
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Block %d set Poisson ratio %3.4g\n",
                         id, elastic.setOfBlocks[id].PoissonRatio);
    }
  }
  // Add Neumann forces
  CHKERR MetaNeummanForces::addNeumannBCElements(m_field, "DISPLACEMENT");

  // Range of flat prisms inserted between slave and master tris
  Range range_slave_master_prisms;

  ContactSearchKdTree contact_search_kd_tree(m_field);

  // Define multi-index conatiner to save these prisms and corresponding tris to
  // be used for numerical integration of contact matrices
  ContactSearchKdTree::ContactCommonData_multiIndex
      contact_commondata_multi_index;

  // create kd_tree with master_surface only
  CHKERR contact_search_kd_tree.buildTree(range_surf_master);

  // Fill this multi-index conainter contact_commondata_multi_index and range
  // range_slave_master_prisms.  inputs  = [range_surf_master, range_surf_slave]
  // outputs = [contact_commondata_multi_index, range_slave_master_prisms]
  // this will be done with kd_tree search, efficient algorithm

  CHKERR contact_search_kd_tree.contactSearchAlgorithm(
      range_surf_master, range_surf_slave, contact_commondata_multi_index,
      range_slave_master_prisms);
  cout << "range_slave_master_prisms = " << range_slave_master_prisms.size()
       << endl;

  // Add these prisim (between master and slave tris) to the mofem database
  EntityHandle meshset_slave_master_prisms;
  CHKERR moab.create_meshset(MESHSET_SET, meshset_slave_master_prisms);

  CHKERR
  moab.add_entities(meshset_slave_master_prisms, range_slave_master_prisms);

  CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
      meshset_slave_master_prisms, 3, bit_level0);
  CHKERR m_field.getInterface<BitRefManager>()->getEntitiesByRefLevel(
      bit_level0, BitRefLevel().set(), meshset_level0);
  CHKERR moab.write_mesh("slave_master_prisms.vtk",
                         &meshset_slave_master_prisms, 1);
  // add contact element to be used to fill C and C^T matrices
  // ContactProblemKdTree contact_problem(m_field,
  // contact_commondata_multi_index);
  ContactProblemSmallDispNoFriction contact_problem(
      m_field, contact_commondata_multi_index, r_value, cn_value);
  contact_problem.addContactElement("CONTACT_ELEM", "DISPLACEMENT", "LAGMULT",
                                    range_slave_master_prisms);

  // build field
  CHKERR m_field.build_fields();

  // build finite elemnts
  CHKERR m_field.build_finite_elements();

  // build adjacencies
  CHKERR m_field.build_adjacencies(bit_level0);

  // define problems
  CHKERR m_field.add_problem("CONTACT_PROB");

  // set refinement level for problem
  CHKERR m_field.modify_problem_ref_level_add_bit("CONTACT_PROB", bit_level0);

  DMType dm_name = "CONTACT_PROB";
  CHKERR DMRegister_MoFEM(dm_name);

  // create dm instance
  DM dm;
  CHKERR DMCreate(PETSC_COMM_WORLD, &dm);
  CHKERR DMSetType(dm, dm_name);

  // set dm datastruture which created mofem datastructures
  CHKERR DMMoFEMCreateMoFEM(dm, &m_field, dm_name, bit_level0);
  CHKERR DMSetFromOptions(dm);
  CHKERR DMMoFEMSetIsPartitioned(dm, is_partitioned);
  // add elements to dm
  CHKERR DMMoFEMAddElement(dm, "ELASTIC");
  CHKERR DMMoFEMAddElement(dm, "FORCE_FE");
  CHKERR DMMoFEMAddElement(dm, "CONTACT_ELEM");
  CHKERR DMSetUp(dm);

  // create vectors and matrices
  Mat Aij;
  Vec d, F_ext;

  {
    CHKERR DMCreateGlobalVector_MoFEM(dm, &d);
    CHKERR VecDuplicate(d, &F_ext);
    CHKERR DMCreateMatrix_MoFEM(dm, &Aij);
    CHKERR VecZeroEntries(d);
    CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMMeshToLocalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
    // TODO: ask, global/local?
    CHKERR MatZeroEntries(Aij);
  }

  bool flag_cubit_disp = false;
  for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
           m_field, NODESET | DISPLACEMENTSET, it)) {
    flag_cubit_disp = true;
  }

  boost::shared_ptr<FEMethod> dirichlet_bc_ptr;
  // if normally defined boundary conditions are not found, try to use
  // DISPLACEMENT blockset
  if (!flag_cubit_disp) {
    dirichlet_bc_ptr =
        boost::shared_ptr<FEMethod>(new DirichletSetFieldFromBlockWithFlags(
            m_field, "DISPLACEMENT", "DISPLACEMENT", Aij, d, F_ext));
    (static_cast<DirichletSetFieldFromBlockWithFlags *>(dirichlet_bc_ptr.get()))
        ->methodsOp.push_back(
            new TimeForceScale("-my_displacements_history", false));
  } else {
    dirichlet_bc_ptr = boost::shared_ptr<FEMethod>(
        new DirichletDisplacementBc(m_field, "DISPLACEMENT", Aij, d, F_ext));
    (static_cast<DirichletDisplacementBc *>(dirichlet_bc_ptr.get()))
        ->methodsOp.push_back(
            new TimeForceScale("-my_displacements_history", false));
  }

  dirichlet_bc_ptr->snes_ctx = SnesMethod::CTX_SNESNONE;
  dirichlet_bc_ptr->snes_x = d;
  CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
  CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);

  contact_problem.setContactOperatorsActiveSet("DISPLACEMENT", "LAGMULT");

  // Rhs
  CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, NULL, dirichlet_bc_ptr.get(),
                                NULL);

  boost::ptr_map<std::string, NeummanForcesSurface> neumann_forces;
  CHKERR MetaNeummanForces::setMomentumFluxOperators(m_field, neumann_forces,
                                                     F_ext, "DISPLACEMENT");
  {
    boost::ptr_map<std::string, NeummanForcesSurface>::iterator mit =
        neumann_forces.begin();
    for (; mit != neumann_forces.end(); ++mit) {
      cout << "Neumman BC " << mit->first.c_str() << endl;
      CHKERR DMMoFEMSNESSetFunction(dm, mit->first.c_str(),
                                    &mit->second->getLoopFe(), NULL, NULL);
    }
  }

  double area_slave, area_master;
  area_master = 0.0;
  area_slave = 0.0;
  contact_problem.setContactOperators("DISPLACEMENT", "LAGMULT", Aij,
                                      contact_commondata_multi_index,
                                      area_master, area_slave);

  CHKERR DMMoFEMSNESSetFunction(dm, "CONTACT_ELEM",
                                &contact_problem.getLoopFeActiveSetRhs(),
                                PETSC_NULL, PETSC_NULL);

  CHKERR DMMoFEMSNESSetFunction(dm, "ELASTIC", &elastic.getLoopFeRhs(),
                                PETSC_NULL, PETSC_NULL);

  CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, NULL, NULL,
                                dirichlet_bc_ptr.get());

  // LHS

  boost::shared_ptr<FEMethod> fe_null;
  CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                fe_null);

  CHKERR DMMoFEMSNESSetJacobian(
      dm, "CONTACT_ELEM", &contact_problem.getLoopFeActiveSetLhs(), NULL, NULL);

  CHKERR DMMoFEMSNESSetJacobian(dm, "ELASTIC", &elastic.getLoopFeLhs(), NULL,
                                NULL);

  CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                dirichlet_bc_ptr);

  // SNES
  SNESConvergedReason snes_reason;
  SNES snes;
  SnesCtx *snes_ctx;
  {
    CHKERR SNESCreate(PETSC_COMM_WORLD, &snes);
    // CHKERR SNESSetDM(snes,dm);
    CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
    CHKERR SNESSetFunction(snes, F_ext, SnesRhs, snes_ctx);
    CHKERR SNESSetJacobian(snes, Aij, Aij, SnesMat, snes_ctx);
    CHKERR SNESSetFromOptions(snes);
  }

  PetscInt d_size, f_ext_size, mat_row, mat_col;
  CHKERR VecGetSize(d, &d_size);
  CHKERR VecGetSize(F_ext, &f_ext_size);
  CHKERR MatGetSize(Aij, &mat_row, &mat_col);

  PostProcVolumeOnRefinedMesh post_proc(m_field);
  // Add operators to the elements, starting with some generic
  CHKERR post_proc.generateReferenceElementMesh();
  CHKERR post_proc.addFieldValuesPostProc("DISPLACEMENT");
  CHKERR post_proc.addFieldValuesGradientPostProc("DISPLACEMENT");

  // add post-processing for stresses
  post_proc.getOpPtrVector().push_back(new PostProcHookStress(
      m_field, post_proc.postProcMesh, post_proc.mapGaussPts, "DISPLACEMENT",
      post_proc.commonData, &elastic.setOfBlocks));

  Vec d0;
  CHKERR VecDuplicate(d, &d0);
  CHKERR DMoFEMMeshToLocalVector(dm, d0, INSERT_VALUES, SCATTER_FORWARD);
  double step_size = 1. / nb_sub_steps;

  double t = 0;
  double delta_time = 0.1;
  for (int ss = 1; ss <= nb_sub_steps; ++ss) {
    t += delta_time;
    dirichlet_bc_ptr->ts_t = t;

    VecAssemblyBegin(d);
    ierr = VecAssemblyEnd(d);
    ierr = VecCopy(d, d0);

    CHKERR VecAXPY(d, step_size, d0);
    CHKERR DMoFEMMeshToLocalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
    CHKERR SNESSolve(snes, PETSC_NULL, d);
    CHKERR SNESGetConvergedReason(snes, &snes_reason);
    int its;
    CHKERR SNESGetIterationNumber(snes, &its);
    CHKERR PetscPrintf(PETSC_COMM_WORLD, "number of Newton iterations = %D\n\n",
                       its);

    CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);
    // Save data on mesh
    CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", &post_proc);

    string out_file_name;
    std::ostringstream stm;
    stm << "out_" << ss << ".h5m";
    out_file_name = stm.str();
    CHKERR
    PetscPrintf(PETSC_COMM_WORLD, "out file %s\n", out_file_name.c_str());

    CHKERR post_proc.postProcMesh.write_file(out_file_name.c_str(), "MOAB",
                                             "PARALLEL=WRITE_PART");
  }

  CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR DMoFEMMeshToGlobalVector(dm, d, INSERT_VALUES, SCATTER_REVERSE);

  CHKERR VecDestroy(&d0);

  std::string wait;
  std::cin >> wait;
  cerr << " RHS vector \n";

  CHKERR SNESDestroy(&snes);

  CHKERR MatDestroy(&Aij);
  CHKERR VecDestroy(&d);
  CHKERR VecDestroy(&F_ext);

  CHKERR DMDestroy(&dm);

  MoFEM::Core::Finalize();
  return 0;
}